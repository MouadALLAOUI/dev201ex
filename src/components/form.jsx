function MyForm() {
  return (
    <div className="qui-img" id="contact">
      <section className="equipe">
        <form action="">
          <div className="email form-input">
            <label htmlFor="email">Email</label>
            <input type="email" name="email" id="email" />
          </div>
          <div className="nom form-input">
            <label htmlFor="nom">nom</label>
            <input type="text" name="nom" id="nom" />
          </div>
          <div className="message form-input">
            <label htmlFor="message">message</label>
            <textarea name="message" id="message" />
          </div>
          <button type="submit" className="btn btn-primary">
            send
          </button>
        </form>
      </section>
    </div>
  );
}
export default MyForm;
