import mem1 from "../assets/angry.png";
import mem2 from "../assets/angry1.png";
import mem3 from "../assets/liza home shoked.png";

function Equipe() {
  return (
    <div className="qui-img">
      <section className="equipe">
        <h3>equipe</h3>
        <div className="members">
          <div className="member">
            <img src={mem1} alt="member" />
            <h3>Mouad</h3>
            <h5>Front-end dev</h5>
            <p>i work on the interface</p>
          </div>
          <div className="member">
            <img src={mem2} alt="member" />
            <h3>Ali</h3>
            <h5>Back-end dev</h5>
            <p>i built the database</p>
          </div>
          <div className="member">
            <img src={mem3} alt="member" />
            <h3>Fatima</h3>
            <h5>designer</h5>
            <p>i propose the styling and color combination</p>
          </div>
        </div>
      </section>
    </div>
  );
}
export default Equipe;
