import Quiimage from "../assets/parentbedroom.png";
function Qui_Img() {
  return (
    <div className="qui-img" id="qui">
      <section className="qui">
        <h3>Qui?</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis
          repellat ullam asperiores iusto non! Consectetur ipsam praesentium
          aperiam sequi, expedita, ratione enim explicabo impedit illo veniam
          fuga nemo, neque nisi.
        </p>
      </section>
      <section className="image">
        <img src={Quiimage} alt="img" />
      </section>
    </div>
  );
}
export default Qui_Img;
