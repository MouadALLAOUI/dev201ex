import Equipe from "./components/equipe";
import MyForm from "./components/form";
import My_nav from "./components/nav";
import Qui_Img from "./components/qui_img";
import Myslide from "./components/slider";

import Back from './assets/bg meadow.png'

function App() {
  return (
    <div className="App">
      <div className="background">
        <img src={Back} alt="background" />
      </div>
      <My_nav />
      <Myslide />
      <Qui_Img />
      <Equipe />
      <MyForm />
    </div>
  )
}

export default App;
